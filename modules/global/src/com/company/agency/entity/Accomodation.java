package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import javax.persistence.OneToMany;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorColumn;
import com.haulmont.chile.core.annotations.Composition;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "AGENCY_ACCOMODATION")
@Entity(name = "agency$Accomodation")
public class Accomodation extends StandardEntity {
    private static final long serialVersionUID = -8606218750453050067L;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "CHECK_IN_DATE", nullable = false)
    protected Date checkInDate;

    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "accomodation")
    protected List<Group> groups;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(DeletePolicy.CASCADE)
    @JoinColumn(name = "HOTEL_ID")
    protected Hotel hotel;

    @Temporal(TemporalType.DATE)
    @Column(name = "CHECK_OUT_DATE")
    protected Date checkOutDate;


    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Hotel getHotel() {
        return hotel;
    }


    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Group> getGroups() {
        return groups;
    }


    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }


}