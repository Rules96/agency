package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import javax.validation.constraints.Future;
import javax.validation.constraints.Past;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.chile.core.annotations.MetaProperty;
import javax.persistence.Transient;
import com.haulmont.cuba.security.entity.SessionAction;
import com.haulmont.cuba.core.app.dynamicattributes.PropertyType;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.List;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

@NamePattern("%s|id")
@Table(name = "AGENCY_GROUP")
@Entity(name = "agency$Group")
public class Group extends StandardEntity {
    private static final long serialVersionUID = -2985255397765197708L;

    @Min(0)
    @Column(name = "N_PLACES")
    protected Integer numberOfPlaces;

    @JoinTable(name = "AGENCY_GROUP_CLIENT_LINK",
        joinColumns = @JoinColumn(name = "GROUP_ID"),
        inverseJoinColumns = @JoinColumn(name = "CLIENT_ID"))
    @ManyToMany
    protected List<Client> clients;

    @Future
    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DEPARTURE_DATE", nullable = false)
    protected Date departureDate;

    @Future
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "RETURN_DATE", nullable = false)
    protected Date returnDate;








    @Lookup(type = LookupType.DROPDOWN, actions = {"lookup", "open", "clear"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOMODATION_ID")
    protected Accomodation accomodation;

    public void setAccomodation(Accomodation accomodation) {
        this.accomodation = accomodation;
    }

    public Accomodation getAccomodation() {
        return accomodation;
    }


    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Client> getClients() {
        return clients;
    }


    public void setNumberOfPlaces(Integer numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public Integer getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }


}