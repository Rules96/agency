package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@NamePattern("%s|name")
@Table(name = "AGENCY_COUNTRY")
@Entity(name = "agency$Country")
public class Country extends StandardEntity {
    private static final long serialVersionUID = -3595507917323104812L;

    @Column(name = "NAME")
    protected String name;

    @JoinTable(name = "AGENCY_TRIP_COUNTRY_LINK",
        joinColumns = @JoinColumn(name = "COUNTRY_ID"),
        inverseJoinColumns = @JoinColumn(name = "TRIP_ID"))
    @ManyToMany
    protected List<Trip> trips;

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public List<Trip> getTrips() {
        return trips;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}