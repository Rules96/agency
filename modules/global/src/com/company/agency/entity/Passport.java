package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|fullname")
@Table(name = "AGENCY_PASSPORT")
@Entity(name = "agency$Passport")
public class Passport extends StandardEntity {
    private static final long serialVersionUID = 9000098085018370318L;

    @Column(name = "FULLNAME")
    protected String fullname;

    @Column(name = "FOREIGN_PASSPORT_DATA")
    protected String foreignPassportData;

    public void setForeignPassportData(String foreignPassportData) {
        this.foreignPassportData = foreignPassportData;
    }

    public String getForeignPassportData() {
        return foreignPassportData;
    }


    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullname() {
        return fullname;
    }


}