package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import com.haulmont.cuba.core.entity.StandardEntity;
import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Table(name = "AGENCY_TRIP")
@Entity(name = "agency$Trip")
public class Trip extends StandardEntity {
    private static final long serialVersionUID = 7960261537738164229L;

    @Min(0)
    @Column(name = "DURATION", nullable = false)
    protected Integer duration;

    @JoinTable(name = "AGENCY_TRIP_TRANSPORT_TYPE_LINK",
        joinColumns = @JoinColumn(name = "TRIP_ID"),
        inverseJoinColumns = @JoinColumn(name = "TRANSPORT_TYPE_ID"))
    @ManyToMany
    protected List<TransportType> transportTypes;

    @DecimalMin("0")
    @NotNull
    @Column(name = "COST", nullable = false)
    protected BigDecimal cost;


    @JoinTable(name = "AGENCY_TRIP_COUNTRY_LINK",
        joinColumns = @JoinColumn(name = "TRIP_ID"),
        inverseJoinColumns = @JoinColumn(name = "COUNTRY_ID"))
    @ManyToMany
    protected List<Country> countries;

    public void setTransportTypes(List<TransportType> transportTypes) {
        this.transportTypes = transportTypes;
    }

    public List<TransportType> getTransportTypes() {
        return transportTypes;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<Country> getCountries() {
        return countries;
    }


    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }



    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDuration() {
        return duration;
    }


}