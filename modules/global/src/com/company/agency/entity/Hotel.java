package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|name")
@Table(name = "AGENCY_HOTEL")
@Entity(name = "agency$Hotel")
public class Hotel extends StandardEntity {
    private static final long serialVersionUID = 1179962375517497521L;

    @Column(name = "NAME")
    protected String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}