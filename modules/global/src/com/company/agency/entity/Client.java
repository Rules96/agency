package com.company.agency.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import java.util.List;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@NamePattern("%s|passport")
@Table(name = "AGENCY_CLIENT")
@Entity(name = "agency$Client")
public class Client extends StandardEntity {
    private static final long serialVersionUID = 2947944567358482581L;

    @NotNull
    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "PASSPORT_ID")
    protected Passport passport;

    @JoinTable(name = "AGENCY_GROUP_CLIENT_LINK",
        joinColumns = @JoinColumn(name = "CLIENT_ID"),
        inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
    @ManyToMany
    protected List<Group> groups;

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Group> getGroups() {
        return groups;
    }


    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public Passport getPassport() {
        return passport;
    }


}