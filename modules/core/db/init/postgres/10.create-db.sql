-- begin AGENCY_TRANSPORT_TYPE
create table AGENCY_TRANSPORT_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    --
    primary key (ID)
)^
-- end AGENCY_TRANSPORT_TYPE
-- begin AGENCY_TRIP
create table AGENCY_TRIP (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DURATION integer not null,
    COST decimal(19, 2) not null,
    --
    primary key (ID)
)^
-- end AGENCY_TRIP
-- begin AGENCY_COUNTRY
create table AGENCY_COUNTRY (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    --
    primary key (ID)
)^
-- end AGENCY_COUNTRY
-- begin AGENCY_GROUP
create table AGENCY_GROUP (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    N_PLACES integer,
    DEPARTURE_DATE date not null,
    RETURN_DATE date not null,
    ACCOMODATION_ID uuid,
    --
    primary key (ID)
)^
-- end AGENCY_GROUP
-- begin AGENCY_PASSPORT
create table AGENCY_PASSPORT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FULLNAME varchar(255),
    FOREIGN_PASSPORT_DATA varchar(255),
    --
    primary key (ID)
)^
-- end AGENCY_PASSPORT
-- begin AGENCY_CLIENT
create table AGENCY_CLIENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    PASSPORT_ID uuid not null,
    --
    primary key (ID)
)^
-- end AGENCY_CLIENT
-- begin AGENCY_ACCOMODATION
create table AGENCY_ACCOMODATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CHECK_IN_DATE date not null,
    HOTEL_ID uuid,
    CHECK_OUT_DATE date,
    --
    primary key (ID)
)^
-- end AGENCY_ACCOMODATION
-- begin AGENCY_HOTEL
create table AGENCY_HOTEL (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    --
    primary key (ID)
)^
-- end AGENCY_HOTEL
-- begin AGENCY_GROUP_CLIENT_LINK
create table AGENCY_GROUP_CLIENT_LINK (
    CLIENT_ID uuid,
    GROUP_ID uuid,
    primary key (CLIENT_ID, GROUP_ID)
)^
-- end AGENCY_GROUP_CLIENT_LINK
-- begin AGENCY_TRIP_TRANSPORT_TYPE_LINK
create table AGENCY_TRIP_TRANSPORT_TYPE_LINK (
    TRIP_ID uuid,
    TRANSPORT_TYPE_ID uuid,
    primary key (TRIP_ID, TRANSPORT_TYPE_ID)
)^
-- end AGENCY_TRIP_TRANSPORT_TYPE_LINK
-- begin AGENCY_TRIP_COUNTRY_LINK
create table AGENCY_TRIP_COUNTRY_LINK (
    COUNTRY_ID uuid,
    TRIP_ID uuid,
    primary key (COUNTRY_ID, TRIP_ID)
)^
-- end AGENCY_TRIP_COUNTRY_LINK
