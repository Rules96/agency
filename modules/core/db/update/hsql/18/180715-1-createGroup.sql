create table AGENCY_GROUP (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    N_PLACES integer,
    DEPARTURE_DATE date not null,
    RETURN_DATE date not null,
    ACCOMODATION_ID varchar(36),
    --
    primary key (ID)
);
