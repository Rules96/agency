create table AGENCY_TRIP_COUNTRY_LINK (
    COUNTRY_ID varchar(36) not null,
    TRIP_ID varchar(36) not null,
    primary key (COUNTRY_ID, TRIP_ID)
);
