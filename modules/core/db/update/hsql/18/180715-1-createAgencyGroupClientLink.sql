create table AGENCY_GROUP_CLIENT_LINK (
    CLIENT_ID varchar(36) not null,
    GROUP_ID varchar(36) not null,
    primary key (CLIENT_ID, GROUP_ID)
);
