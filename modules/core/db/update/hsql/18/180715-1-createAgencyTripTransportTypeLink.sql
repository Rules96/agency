create table AGENCY_TRIP_TRANSPORT_TYPE_LINK (
    TRIP_ID varchar(36) not null,
    TRANSPORT_TYPE_ID varchar(36) not null,
    primary key (TRIP_ID, TRANSPORT_TYPE_ID)
);
