alter table AGENCY_GROUP_CLIENT_LINK add constraint FK_GROCLI_ON_CLIENT foreign key (CLIENT_ID) references AGENCY_CLIENT(ID);
alter table AGENCY_GROUP_CLIENT_LINK add constraint FK_GROCLI_ON_GROUP foreign key (GROUP_ID) references AGENCY_GROUP(ID);
