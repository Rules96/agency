create table AGENCY_TRIP_COUNTRY_LINK (
    COUNTRY_ID uuid,
    TRIP_ID uuid,
    primary key (COUNTRY_ID, TRIP_ID)
);
