create table AGENCY_ACCOMODATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CHECK_IN_DATE date not null,
    HOTEL_ID uuid,
    CHECK_OUT_DATE date,
    --
    primary key (ID)
);
