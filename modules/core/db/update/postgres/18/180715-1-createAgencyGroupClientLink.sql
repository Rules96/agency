create table AGENCY_GROUP_CLIENT_LINK (
    CLIENT_ID uuid,
    GROUP_ID uuid,
    primary key (CLIENT_ID, GROUP_ID)
);
