alter table AGENCY_TRIP_COUNTRY_LINK add constraint FK_TRICOU_ON_COUNTRY foreign key (COUNTRY_ID) references AGENCY_COUNTRY(ID);
alter table AGENCY_TRIP_COUNTRY_LINK add constraint FK_TRICOU_ON_TRIP foreign key (TRIP_ID) references AGENCY_TRIP(ID);
