create table AGENCY_TRIP_TRANSPORT_TYPE_LINK (
    TRIP_ID uuid,
    TRANSPORT_TYPE_ID uuid,
    primary key (TRIP_ID, TRANSPORT_TYPE_ID)
);
